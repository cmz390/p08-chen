//
//  main.m
//  p08-chen
//
//  Created by MingzhaoChen on 5/7/17.
//  Copyright © 2017 MingzhaoChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
