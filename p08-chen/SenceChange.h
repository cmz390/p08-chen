//
//  SenceChange.h
//  p08-chen
//
//  Created by MingzhaoChen on 5/8/17.
//  Copyright © 2017 MingzhaoChen. All rights reserved.
//

#ifndef SenceChange_h
#define SenceChange_h



#import <SpriteKit/SpriteKit.h>

@interface SenceChange : SKScene

-(id)initWithSize:(CGSize)size won:(BOOL)won;

@end
#endif /* SenceChange_h */
